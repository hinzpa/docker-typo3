# About this Repo

This is a Git repo of a TYPO3 Docker image.

# Supported tags
6.2, 6.2-latest
7, 7-latest
8, 8-latest
master
latest

# Imagename
phinz/typo3:latest

# Docker Compose example
```
version: "2"
services:
  web:
    image: phinz/typo3:latest
    ports:
      - "8080:80"
    depends_on:
      - db

  db:
    image: mysql:5.7
    environment:
      - "MYSQL_ROOT_PASSWORD=password"
      - "MYSQL_DATABASE=typo3"
      - "MYSQL_USER=username"
      - "MYSQL_PASSWORD=password"
```